import subprocess
import binascii
import argparse
import struct
import random
import sys
import os


# Bitwise Operations on Chars
ROL = lambda char, val: chr((ord(char) << (val % 8) | ord(char) >> (8 - (val % 8))) & 0xff)
ROR = lambda char, val: chr((ord(char) >> (val % 8) | ord(char)<< (8 - (val % 8))) & 0xff)
XOR = lambda char, val: chr((ord(char) ^ val) & 0xff) 
NOT = lambda char: chr((~ord(char) & 0xff))


def dump_hex(data):
    hstr = ""
    for char in data:
        new = hex(ord(char))[2:]
        if len(new) != 2:
            new = '0' + new
        hstr += "\\x" + new
    return hstr
    
    
def dump_db(data):
    hstr = ""
    for char in data:
        new = hex(ord(char))[2:]
        if len(new) != 2:
            new = '0' + new
        hstr += "0x" + new + ','
    return hstr[:-1]
    

def encode_grouping(grouping, rolval, badchars):
    goodchars = [x for x in range(256) if chr(x) not in badchars]
    random.shuffle(goodchars)
    complete = False
        
    while goodchars and not complete:
        complete = True
        encoded = ""
        rchar = goodchars.pop()
        encoded += chr(rchar)
        
        for char in grouping:
            new = XOR(char, rchar)
            new = ROL(new, rolval)
            if new in badchars:
                complete = False
                break
            encoded += new
            
        if complete:
            return encoded
    return None
        
        
def encode_shellcode(shellcode, groupsize, rolval, badchars):
    start = 0
    encoded = ""
    badchars.append('\xff')
    
    while start < len(shellcode):
        curr = shellcode[start:start + groupsize]
        curr_encoded = encode_grouping(curr, rolval, badchars)
        if not curr_encoded:
            return None
        encoded += curr_encoded
        start += groupsize
    encoded += '\xff'
    return encoded
    

def generate_from_template(encoded, groupsize, rolval):
    fullpath = os.path.abspath("decode-template.asm")
    with open(fullpath, "r") as f:
        template = f.read()
        
    template = template.replace("{RV}", hex(rolval))
    template = template.replace("{GS}", hex(groupsize))
    template = template.replace("{SHELLCODE}", dump_db(encoded))
    
    tmppath = os.path.join(os.path.dirname(fullpath), "encodeddata.asm")
    with open(tmppath, "w+") as f:
        f.write(template)
        
    encoded = compile_asm(tmppath)
    if not encoded:
        return None
    
    return encoded
        
       
def compile_asm(file):
    fullpath = os.path.abspath(file)

    if not os.path.isfile(fullpath):
        return None
        
    tmppath = os.path.join(os.path.dirname(fullpath), "tmpbin")
    command = ['nasm', '-f', 'bin', '-o', tmppath, fullpath]
    ret = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    ret.wait()
    
    if ret.returncode:
        err = ret.stderr.read()
        print err
        return None
    
    with open(tmppath, "rb") as f:
        data = f.read()
        
    os.remove(tmppath)
    return data
    
    
def main(file, encode, groupsize, rolval, badchars):
    if groupsize < 1:
        print "[-] Group Size must be from 1 - 255"
        return None

    print "[+] Assembling the shellcode in {}".format(file)
    
    rawsc = compile_asm(file)
    if not rawsc:
        print "[-] Failed to compile the original shellcode"
        return None
    
    if encode:
        print "[+] Encoding the shellcode"
        encoded = encode_shellcode(rawsc, groupsize, rolval, badchars)
        if not encoded:
            print "[-] Failed to encode the shellcode"
            return None
            
        print  "[+] Generating the decode shellcode template"
        fullencoded = generate_from_template(encoded, groupsize, rolval)
        if not fullencoded:
            print "[-] Failed to generate shellcode with the decoder"
    else:
        fullencoded = rawsc
  
    print "[+] Dumping shellcode hex"
    print "-" * 80
    print dump_hex(fullencoded)
    print "-" * 80
    print "[+] Shellcode length: {}".format(len(fullencoded))
   
    
    

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("file", metavar="FILE", help="File that contains assembly to encode")
    p.add_argument("-e", "--encode", dest="enc", default=False, action="store_true", help="Encode the input shellcode")
    p.add_argument("-g", "--groupsize", dest="gs", type=int, default=4, help="Size of groups (1-255)")
    p.add_argument("-r", "--rolval", dest="rol", type=int, default=4, help="Value to ROL each character (0-255)")
    p.add_argument("-b", "--badchars", dest="bad", nargs="*", default=[], help="Space seperated list of bad characer hex values ex. 00 0a 0c")
    a = p.parse_args()
    
    main(a.file, a.enc, a.gs % 256, a.rol % 256, [binascii.unhexlify(x) for x in a.bad])