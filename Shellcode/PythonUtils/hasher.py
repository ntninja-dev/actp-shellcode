import argparse
import binascii
import struct

ROLVAL = 27

def hash_func(funcname):
    hash = 0
    funcname = funcname.lower()
    for char in funcname:
        val = ord(char)
        hash = (hash << (ROLVAL % 32) | hash >> (32 - (ROLVAL % 32))) & 0xffffffff
        hash += val
    return "0x" + binascii.hexlify(struct.pack(">I", hash))
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("fname", metavar="FUNCNAME", help="Name of the function to hash")
    args = parser.parse_args()
    
    print "%define {}\t\t{}".format(args.fname.lower(), hash_func(args.fname))
    