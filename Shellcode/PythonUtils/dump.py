import sys, os
import subprocess

def compile_sc(fname):
    cfname = fname.split(".")[0] + ".bin"
    cmd = "nasm -f bin -o {} {}".format(cfname, fname)
    subprocess.call(cmd, shell=True)
    return cfname

def delfile(fname):
    os.remove(fname)

def dumpsc(fname):
    with open(fname, 'rb') as f:
        data = f.read()

    sc = ""
    for c in data:
        n = hex(ord(c))
        n = "\\x" + n[2:]
        if len(n[2:]) != 2:
            n = n[:2] + '0' + n[2:]
        sc += n
    return sc


def main(fname):
    cfname = compile_sc(fname)
    sc = dumpsc(cfname)
    delfile(cfname)

    print "#define SCSZ {}\n".format(len(sc) / 4)
    start = 20 * 4
    count = 25 * 4
	
    scstr = 'char shellcode[] = "{}" \\\n'.format(sc[:start])	
    while start < len(sc):
        scstr += '"{}" \\\n'.format(sc[start:start+count])
        start += count
	
    print scstr[:-3] + ';'



if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: {} ASMFILE".format(sys.argv[0])
        sys.exit(0)
    main(sys.argv[1])
