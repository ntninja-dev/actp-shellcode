bits 32

global _start

section .text

_start:                             ; Entry Point
    cld                             ; Set DF=0 (Should already be like that)
    jmp main


; Get the base address of the Kernel32.dll
; __cdecl void *getk32base()
getk32base:
    mov eax, [fs:0x30]              ; PEB address is TEB+0x30, TEB is at fs
    mov eax, [eax + 0x0c]           ; PEB_LDR_DATA address is at PEB + 0x0c
    mov eax, [eax + 0x14]           ; Address of first entry in InMemoryOrderMuduleLis is PEB_LDR_DATA +0x14 (kernelbase.dll)
    mov eax, [eax]                  ; Address of second entry (ntdll.dll)
    mov eax, [eax]                  ; Address of third entry (kernel32.dll)
    mov eax, [eax + 0x10]           ; Base address is 0x10 from the entry, kernel32 base address
    ret


; Pop the address of the function name of the stack
; __cdecl func *GetFuncAddrFromDLLBase(char *funcname);
GetFuncAddrFromDLLBase:
    push ebp                        ; store ebp
    mov ebp, esp                    ; create a new stack frame
    sub esp, 0x18                   ; make room for locals
    push ebx

    mov edi, [ebp+0x8]              ; Get the address of the FuncName String
    xor eax, eax
    or ecx, 0xffffffff              ; set ecx to max val
    repnz scasb                     ; increment to null char
    not ecx                         ; flip ecx to get strlen
    mov [ebp-0x4], ecx              ; var4 = strlen of FuncName                                            

    call getk32base
    mov ebx, eax                    ; Move kernel32 base address to ebx
    mov [ebp-0x8], ebx              ; var8

    mov eax, [ebx + 0x3c]           ; RVA of PESignature
    add eax, ebx                    ; Address of PESignature = BaseAddr + Sig RVA
    mov eax, [eax + 0x78]           ; RVA of Export Table
    add eax, ebx                    ; Address of ExportTable = BaseAddr + ExpTable RVA

    mov ecx, [eax+0x24]             ; RVA of OrdinalTable
    add ecx, ebx                    ; Address of OrdinalTable
    mov [ebp-0x0c], ecx             ; var12

    mov ecx, [eax+0x20]             ; RVA of NamePointerTable
    add ecx, ebx                    ; Address of NamePointerTable
    mov [ebp-0x10], ecx             ; var16

    mov ecx, [eax+0x1C]             ; RVA of Address Table
    add ecx, ebx                    ; Address of AddressTable
    mov [ebp-0x14], ecx             ; var20

    mov edx, [eax+0x14]             ; Move count of exports to edx
    xor eax, eax                    ; EAX Becomes Counter

    .findfuncloop:
        
        mov esi, [ebp+0x8]          ; arg8 (Address of FunctionName)        

        mov edi, [ebp-0x10]         ; Address of NamePointerTable
        mov edi, [edi + eax * 4]    ; Get the RVA of the Name of our current count (EAX)
        add edi, ebx                ; Get the address of the current name to check

        mov ecx, [ebp-4]            ; get the strlen of the func to find
        repe cmpsb                  ; Compare the strings

        jz GetFuncAddrFromDLLBase.found

        inc eax
        cmp eax, edx                ; check fi we reached all exports
        
        jb GetFuncAddrFromDLLBase.findfuncloop
        jmp GetFuncAddrFromDLLBase.end
        
    .found:
        mov ecx, [ebp-0x0c]         ; var12 = Address of Ordinal Table 
        mov edx, [ebp-0x14]         ; var20 = Address of Address Table

        mov ax, [ecx + eax * 2]     ; OrdinalNumber = var12 + (counter * 2)
        mov eax, [edx + eax * 4]    ; RVA of Function = var20 + (Ordinal * 4)
        add eax, ebx                ; Function Address = base address + Function RVA

    .end:
        pop ebx                     ; restore ebx
        add esp, 0x18               ; cleanup stack
        pop ebp
        ret                         ; clean up stackframe and pointer


; MAIN RUNNING
main:
    xor eax, eax
    jmp strings
    
gotstrs:
    pop ebx
    push ebx
    call GetFuncAddrFromDLLBase
    int3

strings: 
    call gotstrs
    db "LoadLibraryA", 0            ; strings+0
    db "GetProcAddress", 0          ; strings+13