; DWORD __stdcall hash_module(wchar *fullmodulepath);
; Function takes in a pointer to the full_module path located
; in the LDR_DATA_TABLE_ENTRY. The Function will hash ONLY
; the module name.
hash_module:
    ; Set up stack frame
    push ebp
    mov ebp, esp
    push ebx
    push esi
    push edi
    
    ; Find the end of the string
    xor eax, eax
    or ecx, 0xffffffff
    mov edi, [ebp+0x8]
    repnz scasw
    
    ; Move back to the first '\'
    std
    mov al, 0x5c
    repnz scasb
    cld
    
    ; Move the address of just the module name into ebx
    lea ebx, [edi + 3]
    xor eax, eax
    
    .hashloop:
        xor edx, edx
        mov byte dl, [ebx]              ; move the char into edx
        test dl, dl                     ; jmp to done if null
        jz .done
        or dl, 0x20                     ; Make all lower case
        rol eax, ROLVAL                 ; rol the hash by ROLVAL
        add eax, edx                    ; add the new char
        inc ebx                         ; increment ebx twice (UTF16-LE)
        inc ebx
        jmp short .hashloop
    
    .done:
        ; Clean up stack
        pop edi
        pop esi
        pop ebx
        pop ebp
    ret 0x4
    
; DWORD __stdcall hash_proc(char *funcname);
; Function takes in a pointer to the function name
; and returns a hash of the name.
hash_proc:
    ; get the argument
    mov ecx, [esp+4]
    xor eax, eax
    
    .loop:
        xor edx, edx
        mov byte dl, [ecx]              ; mov char into edx
        test dl, dl                     ; stop if null
        jz .done
        or dl, 0x20                     ; make the char lowercase
        rol eax, ROLVAL                 ; ROL the hash
        add eax, edx                    ; add in the new char
        inc ecx                         ; increment to next char
        jmp short .loop
        
    .done:
    ret 0x4
    
    
; HANDLE __stdcall getmodulehandle(DWORD hash);
; Takes in a hash of the module name (CASE SENSITIVE), including the extension (.dll, .exe, etc).
; Returns the handle of the module that is requested.
getmodulehandle:
    push ebp
    mov ebp, esp
    push ebx
    push esi
    
    mov ebx, [fs:0x30]              ; PPEB = Value at TEB+0x30
    mov ebx, [ebx + 0x0c]           ; PPEB_LDR_DATA = Value at PEB+0x0c 
    mov ebx, [ebx + 0x14]           ; PLDR_DATA_ENTRY_TABLE.LIST_ENTRY = PEB_LDR_DATA+0x14 
    mov esi, ebx                    ; Store the head for later
    mov ebx, [ebx]                  ; Get first module
    
    .findmoduleloop:
        mov ecx, [ebx + 0x20]       ; UNICODE_STRING.Buffer =  PLDR_DATA_ENTRY_TABLE.LIST_ENTRY+0x20
        push ecx
        call hash_module
 
        mov ecx, [ebp+0x8]          ; Get the input hash
        cmp eax, ecx                ; check to see if we match
        je .found
        xor eax, eax
        mov ebx, [ebx]              ; get the next entry
        cmp esi, [ebx]              ; Check if the next entry is the head
        je .done
        jmp .findmoduleloop
        
    .found:
        mov eax, [ebx+0x10]         ; Move the module handle to return val
    
    .done:
        pop esi
        pop ebx
        pop ebp
    ret 0x4
    
; FUNCPOINTER __stdcall getprocaddress(handle module [ebp+0x8], dword hash [ebp+0xc]);
; Finds the function with the given hash in the given module
getprocaddress:
    push ebp
    mov ebp, esp
    sub esp, 0xc                    ; Create room for locals
    
    push edi
    push esi
    push ebx
    
    mov ebx, [ebp+0x8]
    mov eax, [ebx+0x3c]             ; RVA of IMAGE_NT_HEADERS
    add eax, ebx                    ; Virtual Address of IMAGE_NT_HEADERS
    mov eax, [eax+0x78]             ; RVA of export table
    add eax, ebx                    ; Virtual Address of Export Table
    
    mov ecx, [eax+0x24]             ; RVA of OrdinalTable
    add ecx, ebx                    ; Virtual Address of Ordinal Table
    mov [ebp-0x4], ecx                ; var_4 = VA of Ordinal Table
    
    mov ecx, [eax+0x20]             ; RVA of Name Pointer Table
    add ecx, ebx                    ; Virtual Address of Name Pointer Table
    mov [ebp-0x8], ecx                ; var_8 = VA of Name Pointer Table
    
    mov ecx, [eax+0x1c]             ; RVA of Function Address Table
    add ecx, ebx                    ; Virtual Address of Fucntion Address Table
    mov [ebp-0xc], ecx              ; var_c = VA of Fucntion Address Table
    
    mov esi, [eax+0x14]             ; Count of Exports
    xor edi, edi                    ; counter
    
    .findfuncloop:
        mov eax, [ebp-0x8]          ; Address of name pointer table 
        mov eax, [eax + edi * 4]    ; RVA of current function name
        add eax, ebx                ; Virtual Address of current function name
        
        push eax    
        call hash_proc              ; get the hash of the process
        
        mov ecx, [ebp+0xc]          ; get input heap
        cmp eax, ecx
        je .found
        
        inc edi
        cmp edi, esi                ; check to see if we have looked at all exports
        jb .findfuncloop
        xor eax, eax
        jmp short .done
        
    .found:
        mov edx, [ebp-0x4]          ; get var_4 = VA of Ordinal Table
        mov ecx, [ebp-0xc]          ; get var_c = VA of Function Address Table
        xor eax, eax
        
        mov ax, [edx + edi * 2]     ; Get the Ordninal of the function
        mov eax, [ecx + eax * 4]    ; Get the RVA of the Function
        add eax, ebx                ; VA of the function
        
    .done:
        pop ebx
        pop esi
        pop edi
        add esp, 0xc
        pop ebp
    ret 0x8
    
; Get the base address of the Kernel32.dll based on position
; Good for use when only kernel32 is needed
; __cdecl void *getk32base()
getk32base:
    mov eax, [fs:0x30]              ; PEB address is TEB+0x30, TEB is at fs
    mov eax, [eax + 0x0c]           ; PEB_LDR_DATA address is at PEB + 0x0c
    mov eax, [eax + 0x14]           ; Address of head of InMemoryOrderMuduleList is PEB_LDR_DATA +0x14
    mov eax, [eax]                  ; Address of first entry (ntdll.dll)
    mov eax, [eax]                  ; Address of second entry (kernel32.dll)
    mov eax, [eax + 0x10]           ; Base address is 0x10 from the entry, kernel32 base address
    ret